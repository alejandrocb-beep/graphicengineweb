import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { HomePage } from './home.page';
import { HomePageRoutingModule } from './home-routing.module';
import { HttpClient, HttpClientModule} from "@angular/common/http";

import { FormatFileSizePipe } from './format-file-size.pipe';

@NgModule({
  imports: [
    HttpClientModule,
    CommonModule,
    FormsModule,
    IonicModule,
    HomePageRoutingModule
  ],
  declarations: [HomePage, FormatFileSizePipe],
  providers: [HttpClient, HttpClientModule]
  
})
export class HomePageModule {}
