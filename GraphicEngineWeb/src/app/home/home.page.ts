import { Component, OnInit, Inject } from '@angular/core';
import { DOCUMENT } from '@angular/common';
//HTTP para los archivos
import { HttpClient } from "@angular/common/http";

//LOGIN
import { FirebaseService } from '../services/firebase.service';
import { UsersService } from '../services/users.service';
import { AngularFireAuth } from '@angular/fire/auth';
import firebase from "firebase/app";
import "firebase/auth";
import "firebase/firestore";

//SUBIR FOTOS Y FICHEROS THANKS TO https://javascript.plainenglish.io/upload-files-and-images-to-firebase-and-retrieve-a-downloadable-url-a5b3467bb89c
import { Observable } from 'rxjs';
import { finalize, tap } from 'rxjs/operators';
import { AngularFireStorage, AngularFireUploadTask } from '@angular/fire/storage';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { AngularFireDatabase, AngularFireList } from '@angular/fire/database';

//TOAST
import { ToastController } from '@ionic/angular';
import { ThrowStmt } from '@angular/compiler';


//INTERFAZ USUARIOS
export interface users {

  nom: string;
  correu: string;
  objetos: objetos[];
  licencia: string;
}

export interface usuarioLicencia {

  correo: string;
  licencia: number;
  expira: string;
  tipus: string;
}

//INTERFAZ FORO MENSAJES

export interface mensajes {

  usuario: string;
  mensaje: string;

}

//INTERFAZ FORO THREADS
export interface threads {

  creador: string;
  titol: string;
  mensajes: mensajes[];

}

//INTERFAZ FOTOS
export interface fotos {
  nom: string;
  path: string;
}

//INTERFAZ OBJETOS
export interface objetos {
  nom: string;
  path: string;
  miniatura: string;
}

var provider = new firebase.auth.GoogleAuthProvider();


@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit{

  private file: File;

  constructor( private afs: AngularFirestore, private db: AngularFireDatabase, private fireStorage: AngularFireStorage, @Inject(DOCUMENT) private _document: Document,
  public toastController: ToastController, private http: HttpClient, private usersService: UsersService, private afAuth: AngularFireAuth,
  private firebaseService: FirebaseService ) {

    this.newUser = {} as users;
    this.newObjeto = {} as objetos;
    this.newFoto = {} as fotos;
    this.newUserLicencia = {} as usuarioLicencia;
    this.userLicenciaData = {} as usuarioLicencia;
    this.messageHelper = {} as mensajes;
    this.nouFil = {} as threads;
  }

  basePathFitxers = '/fitxers';
  basePath = '/images';
  downloadableURL = '';
  task: AngularFireUploadTask;

  progressValue: Observable<number>;

  ngOnInit() {
    this.firebaseService.read("UsuarioLicencia").subscribe(data => {

      this.licenciasList = data.map(e => {
        return {
          id: e.payload.doc.id,
          isEdit: false,
          correo: e.payload.doc.data()['correo'],
          licencia: e.payload.doc.data()['licencia'],
          expira: e.payload.doc.data()['expira']
        };
      })
      console.log(this.licenciasList);
    });

    this.firebaseService.read("fitxers").subscribe(data => {

      this.fitxersList = data.map(e => {
        return {
          id: e.payload.doc.id,
          isEdit: false,
          nom: e.payload.doc.data()['nom'],
          path: e.payload.doc.data()['path'],
          miniatura: e.payload.doc.data()['miniatura']
        };
      })
      console.log(this.fitxersList);
    });

    this.firebaseService.read("threads").subscribe(data => {

      this.threadsList = data.map(e => {
        return {
          id: e.payload.doc.id,
          isEdit: false,
          creador: e.payload.doc.data()['creador'],
          mensajes: e.payload.doc.data()['mensajes'],
          titol: e.payload.doc.data()['titol']
        };
      })
      console.log(this.threadsList);
    });

    this.firebaseService.read("miniaturas").subscribe(data => {

      this.miniaturasList = data.map(e => {
        return {
          id: e.payload.doc.id,
          isEdit: false,
          nom: e.payload.doc.data()['nom'],
          path: e.payload.doc.data()['path']
        };
      })
      console.log(this.miniaturasList);
    });

    this.usersService.read_user().subscribe(data => {

      this.userList = data.map(e => {
        return {
          id: e.payload.doc.id,
          isEdit: false,
          nom: e.payload.doc.data()['nom'],
          correu: e.payload.doc.data()['correu'],
          objetos: e.payload.doc.data()['objetos'],
          licencia: e.payload.doc.data()['licencia']
        };
      })
      console.log(this.userList);
    });
    this.countdown();
  }

  //VARIABLES

  //variables home
  newthread = {mensajes: []};
  logged = false;
  intro = true;
  ishidden = false;
  userData: users;
  newUser: users;
  newUserLicencia: usuarioLicencia;
  userLicenciaData: usuarioLicencia;
  nouFil: threads;
  missatge: string;
  mensajes = [];
  userList = [];
  fotosList = [];
  licenciasList = [];
  fitxersList = [];
  miniaturasList = [];
  threadsList = [];
  dias = 1;
  horas = 20;
  minutos = 30;
  segundos = 19;
  menuToggled= false;

  //variables comprar llicencia
  buying = false;
  loadingBought = false;
  congratulations = false;
  llicenciaComprada = "";

  //variables fitxers
  fitxers = false;
  imagen = true;
  newObjeto: objetos;
  newFoto: fotos;
  fitxersBanner = false;

  //variables forum
  forum = false;
  messageHelper: mensajes;
  titol = "";

  picture;
  name = "Encara no has fet log-in. Inicia sessió.";
  email = "";
  objetos = [];
  licencia = "";
  showMenu = false;



  //FUNCIONES

  toggleMenu(){
    if (this.menuToggled){
      this.menuToggled = false;
    } else {
      this.menuToggled = true;
    }
    
  }

  deleteFitxer(fitxer: objetos) {

    const index = this.userData.objetos.indexOf(fitxer, 0);
    if (index > -1) {
      this.userData.objetos.splice(index, 1);
    }

    this.UpdateUser(this.userData);

    this.reloadFitxers();

    for (let entry of this.fitxersList){
      console.log("De l'arxiu: "+entry);
      console.log("On "+fitxer.path);
      if (entry.path === fitxer.path){
        this.firebaseService.delete(entry.id, "fitxers");
      }
    }

    this.fireStorage.refFromURL(fitxer.path).delete();

  }

  async onFileChangedFitxers(event) {
    const file = event.target.files[0];
    let nameFile = this.getRandomInt(-10000000,1000000)+file.name;
    if (file) {
       const filePath = `${this.basePathFitxers}/${nameFile}`;  // path at which image will be stored in the firebase storage
       this.task =  this.fireStorage.upload(filePath, file);    // upload task

       // this.progress = this.snapTask.percentageChanges();


       (await this.task).ref.getDownloadURL().then(url => {
        this.downloadableURL = url;
        this.newObjeto.nom = file.name;
        this.newObjeto.path = url;
        this.newObjeto.miniatura = ""+this.getRandomInt(-100000000000,100000000);
        this.CreateRecordCollection(this.newObjeto, "fitxers");

        this.userData.objetos.push(this.newObjeto);

        this.UpdateUser(this.userData);

        });



     } else {
       alert('No images selected');
       this.downloadableURL = ''; }


      this.reloadFitxers();
   }

  async onFileChanged(event, nom) {
    console.log(nom);
    const file = event.target.files[0];
    if (file) {
       let nameFile = this.getRandomInt(-1000000000, 100000000)+file.name;
       const filePath = `${this.basePath}/${nameFile}`;  // path at which image will be stored in the firebase storage
       this.task =  this.fireStorage.upload(filePath, file);    // upload task
       this.progressValue = this.task.percentageChanges();
       (await this.task).ref.getDownloadURL().then(url => {
        this.downloadableURL = url;
        this.newFoto.nom = file.name;
        this.newFoto.path = url;
        this.CreateRecordCollection(this.newFoto, "miniaturas");
        console.log("Las miniaturas: ")
        console.log(this.miniaturasList);
        this.reloadMiniaturas();
        for (let thing of this.miniaturasList){
          console.log("PATH QUE LLEGA");
          console.log(nom);
          console.log("PATH MINIATURA");
          console.log(thing.path);
          if (thing.path === nom){
            this.firebaseService.delete(thing.id, "miniaturas");
            this.fireStorage.refFromURL(thing.path).delete();
          }
        }

        let contador = 0;
        for (let entry of this.userData.objetos){
          if (nom === entry.miniatura){
            this.userData.objetos[contador].miniatura = url;
            this.UpdateUser(this.userData);
          }
          contador++;
        }
        });

     } else {
       alert('No images selected');
       this.downloadableURL = ''; }
   }

  goHome() {
    this.loadingBought = false;
    this.congratulations = false;
    this.buying = false;
    this.forum = false;
    this.fitxers = false;
    this.fullyReload();
  }

  goLlicencia() {

    for (let entry of this.licenciasList){
      if(entry.correo === this.userData.correu){
        this.userLicenciaData = entry;
      }
    }


    this.loadingBought = false;
    this.congratulations = false;
    this.buying = true;
    this.forum = false;
    this.fitxers = false;
    this.fullyReload();
  }

  goFitxers() {
    this.loadingBought = false;
    this.congratulations = false;
    this.buying = false;
    this.forum = false;
    this.fitxers = true;
    this.fullyReload();
  }

  goForum() {
    this.loadingBought = false;
    this.congratulations = false;
    this.buying = false;
    this.forum = true;
    this.fitxers = false;
    this.fullyReload();
  }

  reloadMiniaturas(){
    this.firebaseService.read("miniaturas").subscribe(data => {

      this.miniaturasList = data.map(e => {
        return {
          id: e.payload.doc.id,
          isEdit: false,
          nom: e.payload.doc.data()['nom'],
          path: e.payload.doc.data()['path']
        };
      })
      console.log(this.miniaturasList);
    });
  }

  reloadFitxers(){
    this.usersService.read_user().subscribe(data => {

      this.userList = data.map(e => {
        return {
          id: e.payload.doc.id,
          isEdit: false,
          nom: e.payload.doc.data()['nom'],
          correu: e.payload.doc.data()['correu'],
          objetos: e.payload.doc.data()['objetos'],
          licencia: e.payload.doc.data()['licencia']
        };
      })
      console.log(this.userList);
    });

    for (let entry of this.userList){

      if (this.email === entry.correu){
        this.userData = entry;
        break;
      }
    }

  }


  fullyReload() {
    
    this.firebaseService.read("UsuarioLicencia").subscribe(data => {

      this.licenciasList = data.map(e => {
        return {
          id: e.payload.doc.id,
          isEdit: false,
          correo: e.payload.doc.data()['correo'],
          licencia: e.payload.doc.data()['licencia'],
          expira: e.payload.doc.data()['expira']
        };
      })
      console.log(this.licenciasList);
    });

    this.firebaseService.read("fitxers").subscribe(data => {

      this.fitxersList = data.map(e => {
        return {
          id: e.payload.doc.id,
          isEdit: false,
          nom: e.payload.doc.data()['nom'],
          path: e.payload.doc.data()['path'],
          miniatura: e.payload.doc.data()['miniatura']
        };
      })
      console.log(this.fitxersList);
    });

    this.firebaseService.read("threads").subscribe(data => {

      this.threadsList = data.map(e => {
        return {
          id: e.payload.doc.id,
          isEdit: false,
          creador: e.payload.doc.data()['creador'],
          mensajes: e.payload.doc.data()['mensajes'],
          titol: e.payload.doc.data()['titol']
        };
      })
      console.log(this.threadsList);
    });

    this.firebaseService.read("miniaturas").subscribe(data => {

      this.miniaturasList = data.map(e => {
        return {
          id: e.payload.doc.id,
          isEdit: false,
          nom: e.payload.doc.data()['nom'],
          path: e.payload.doc.data()['path']
        };
      })
      console.log(this.miniaturasList);
    });

    this.usersService.read_user().subscribe(data => {

      this.userList = data.map(e => {
        return {
          id: e.payload.doc.id,
          isEdit: false,
          nom: e.payload.doc.data()['nom'],
          correu: e.payload.doc.data()['correu'],
          objetos: e.payload.doc.data()['objetos'],
          licencia: e.payload.doc.data()['licencia']
        };
      })
      console.log(this.userList);
    });
  }

  async updateLlicencia(llicencia: string){

    this.loadingBought = true;
    this.userData.licencia = llicencia;
    this.UpdateUser(this.userData);

    let sk = false;
    for (let entry of this.licenciasList){
      if (this.email === entry.correo){
        sk = true;
        this.newUserLicencia = entry;
        break;
      }
    }

    console.log("Aquí llega");
    if (sk){
      console.log("Lo reconoce como updatear");
      this.newUserLicencia.correo = this.userData.correu;
      this.newUserLicencia.licencia = this.getRandomInt(10000000000, 2471522121212);
      let date = new Date();
      date.setDate(date.getDate() + 30);
      this.newUserLicencia.expira = date.toLocaleString();
      this.newUserLicencia.tipus = llicencia;
      console.log(this.newUserLicencia);
      this.UpdateLicencia(this.newUserLicencia);
      this.encrypt(this.newUserLicencia);
    } else  {
      this.newUserLicencia.correo = this.userData.correu;
      this.newUserLicencia.licencia = this.getRandomInt(10000000000, 2471522121212);
      let date = new Date();
      date.setDate(date.getDate() + 30);
      this.newUserLicencia.expira = date.toLocaleString();
      this.newUserLicencia.tipus = llicencia;
      this.CreateRecordCollection(this.newUserLicencia,"UsuarioLicencia");
      this.encrypt(this.newUserLicencia);
    }




    await this.delay(1000);


    this.loadingBought = false;

    this.buying = false;

    this.congratulations = true;

    this.llicenciaComprada = llicencia;

  }

  getRandomInt(min, max) : number{
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }

  createThread(title: string){
    this.messageHelper.usuario = new Date().toLocaleString() + " | " + this.userData.correu;
    this.messageHelper.mensaje = "Ha creat el fil!";

    this.nouFil.creador = this.userData.correu;
    this.nouFil.titol = new Date().toLocaleString() + " | " + title;
    this.mensajes.push(this.messageHelper);
    this.nouFil.mensajes = this.mensajes;


    this.CreateRecordCollection(this.nouFil, "threads");
    this.alertMensajes();
  }

  submitMessage(id, missatge){

    this.messageHelper.usuario = new Date().toLocaleString() + " | " + this.userData.correu;
    this.messageHelper.mensaje = missatge;

    id.mensajes.push(this.messageHelper);

    this.UpdateThread(id);
    this.alertMensajes();
  }

  deleteThread(id){
    console.log("ESBORRA?")
    this.firebaseService.delete(id.id, "threads");
  }

  async presentToast() {
    const toast = await this.toastController.create({
      message: 'Nom ja utiltizat. Utilitza un altre nom d arxiu.',
      duration: 2000
    });
    toast.present();
  }

  async alertMensajes() {
    const toast = await this.toastController.create({
      message: 'Creat thread i enviat missatge!',
      duration: 2000
    });
    toast.present();
  }

  async alertArxiu() {
    const toast = await this.toastController.create({
      message: 'Enviat arxiu!',
      duration: 2000
    });
    toast.present();
  }


  CreateRecord(user: users) {
    console.log(this.userData);
    this.usersService.create_user(user).then(resp => {

    })
      .catch(error => {
        console.log(error);
      });
  }

  toggle(){
    console.log("pickar"+this.ishidden);
    if (this.ishidden == true){
      this.ishidden = false;
    } else {
      this.ishidden = true;
    }
  }

  async loginGoogle() {
    console.log("1");
    const res = await this.afAuth.signInWithPopup(provider);
    const user = res.user;
    console.log(user);
    this.picture = user.photoURL;
    this.name = user.displayName;
    this.email = user.email;

    let nou = true;
    for (let entry of this.userList){

      if (this.email === entry.correu){
        this.userData = entry;
        nou = false;
        break;
      }
    }

    for (let entry of this.licenciasList){

      if (this.email === entry.correo){
        this.newUserLicencia = entry;
        break;
      }
    }
    console.log(this.userData);
    this.logged = true;
    if (nou){

      this.newUser.nom =  user.displayName;
      this.newUser.correu = user.email;
      this.newUser.objetos =  [];
      this.newUser.licencia = "none";
      this.CreateRecord(this.newUser);
    }

 }

 async tancarGoogle() {
  const res = await this.afAuth.signOut;
  this.picture = 'https://www.simplifai.ai/wp-content/uploads/2019/06/blank-profile-picture-973460_960_720-400x400.png';
  this.name = "Encara no has fet log-in. Inicia sessió.";
  this.email = "";
  this.logged = false;
}


  onFileChange(fileChangeEvent) {
    this.file = fileChangeEvent.target.files[0];
  }

  async submitForm() {
    let formData = new FormData();
    formData.append("photo", this.file, this.file.name+"*"+this.userData.correu);

    console.log("pasa per el post");
    this.http.post("http://localhost:3000/upload", formData).subscribe((response) => {
      console.log(response);
    });
    this.newObjeto.nom = this.file.name;
    this.newObjeto.miniatura = "https://firebasestorage.googleapis.com/v0/b/graphic-engine2021.appspot.com/o/filesStorage%2F1620829032984_beach-summer-sale.jpg?alt=media&token=3ac29de4-81ba-4ff5-8115-6354601c8604";

    this.userData.objetos.push(this.newObjeto);

    console.log("updatea");
    this.UpdateUser(this.userData);
    this.alertArxiu();
    this.reloadFitxers();


  }

  getFile(nom: string) {

    console.log("pasa per el post");
    this.http.get("http://localhost:3000/download", {responseType: 'blob'}).subscribe((response) => {
      this.downloadBlob(nom, response);
      console.log(response);
    });


  }

  //FUNCIÓ FUSILADA DE https://blog.digital-craftsman.de/download-file-through-typescript/ (legacy code, ja no el utilitzo, per si de cas)

  public downloadBlob(fileName: string, blob: Blob): void {
    if (window.navigator.msSaveOrOpenBlob) {
      window.navigator.msSaveBlob(blob, fileName);
    } else {
      const anchor = window.document.createElement('a');
      anchor.href = window.URL.createObjectURL(blob);
      anchor.download = fileName;
      document.body.appendChild(anchor);
      anchor.click();
      document.body.removeChild(anchor);
      window.URL.revokeObjectURL(anchor.href);
    }
  }

  //CRUD

  CreateRecordCollection(record: any, collection: string) {
    this.firebaseService.create(record, collection).then(resp => {
    })
      .catch(error => {
        console.log(error);
      });
  }

  RemoveRecord(rowID, collection: string) {
    this.firebaseService.delete(rowID,collection);
  }


  UpdateUser(recordRow) {
    let record = {};
    record['nom'] = recordRow.nom;
    record['correu'] = recordRow.correu;
    record['objetos'] = recordRow.objetos;
    record['licencia'] = recordRow.licencia;

    this.usersService.update(recordRow.id, record, "users");
  }

  UpdateLicencia(recordRow) {
    let record = {};
    record['correo'] = recordRow.correo;
    record['licencia'] = recordRow.licencia;
    record['expira'] = recordRow.expira;

    this.firebaseService.update(recordRow.id, record, "UsuarioLicencia");
  }

  UpdateThread(recordRow) {
    let record = {};
    record['creador'] = recordRow.creador;
    record['titol'] = recordRow.titol;
    record['mensajes'] = recordRow.mensajes;

    this.firebaseService.update(recordRow.id, record, "threads");
  }

  //función para la cuenta atras
  async countdown(){
    while (true){
      await this.delay(1000);
      this.segundos--;
      if (this.segundos == -1){
        this.segundos = 59;
        this.minutos--;
        if (this.minutos == -1){
          this.minutos = 59;
          this.horas--;
          if (this.horas == -1){
            this.horas = 23
            this.dias--;
            if (this.dias == -1){
              break;
            }
          }
        }
      }
    }
  }

  //sleep
  delay(ms: number) {
    return new Promise( resolve => setTimeout(resolve, ms) );
}

  //encryptar

  encrypt(lic: usuarioLicencia){

    console.log(lic);

    var result = "";
    var message = lic.correo.toLowerCase()+";"+lic.licencia+";"+";"+lic.expira.toLowerCase();
    
    for(let character of message){
      switch(character){
        case "a":
          result+="p";
          break;
        case "b":
          result+="q";
          break;
        case "c":
          result+="o";
          break;
        case "d":
          result+="w";
          break;
        case "e":
          result+="i";
          break;
        case "f":
          result+="e";
          break;
        case "g":
          result+="u";
          break;
        case "h":
          result+="r";
          break;
        case "i":
          result+="y";
          break;
        case "j":
          result+="t";
          break;
        case "k":
          result+="a";
          break;
        case "l":
          result+="k";
          break;
        case "m":
          result+="s";
          break;
        case "n":
          result+="l";
          break;
        case "o":
          result+="f";
          break;
        case "p":
          result+="j";
          break;
        case "q":
          result+="h";
          break;
        case "r":
          result+="g";
          break;
        case "s":
          result+="z";
          break;
        case "t":
          result+="m";
          break;
        case "u":
          result+="x";
          break;
        case "v":
          result+="n";
          break;
        case "w":
          result+="c";
          break;
        case "x":
          result+="b";
          break;
        case "y":
          result+="v";
          break;
        case "z":
          result+="d";
          break;
        case "1":
          result+="5";
          break;
        case "2":
          result+="4";
          break;
        case "3":
          result+="2";
          break;
        case "4":
          result+="1";
          break;
        case "5":
          result+="3";
          break;
        case "6":
          result+="9";
          break;
        case "7":
          result+="8";
          break;
        case "8":
          result+="6";
          break;
        case "9":
          result+="0";
          break;
        case "0":
          result+="7";
          break;
        case ".":
          result+="¿";
          break;
        case "@":
          result+="_";
          break;
        case ":":
          result+="¡";
          break;
        case " ":
          result+="*";
          break;
        case ";":
          result+=";";
          break;
        case "/":
          result+=")";
          break;
        default:
          result+=character;
          break;
      }
    }
    console.log(message+" "+result);
    var fileContents = result;
    var filename = "llicencia.txt";
    var filetype = "text/plain";

    var a = document.createElement("a");
    var dataURI = "data:" + filetype +
        ";base64," + btoa(fileContents);
    a.href = dataURI;
    a['download'] = filename;
    var e = document.createEvent("MouseEvents");
    // Use of deprecated function to satisfy TypeScript.
    e.initMouseEvent("click", true, false,
        document.defaultView, 0, 0, 0, 0, 0,
        false, false, false, false, 0, null);
    a.dispatchEvent(e);
    
  }




}
