// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false
};

export const firebaseConfig = {
  apiKey: "AIzaSyBavMeOfnL5fPqLXeaOWwQoDE7t87vbIUA",
  authDomain: "graphic-engine2021.firebaseapp.com",
  projectId: "graphic-engine2021",
  storageBucket: "graphic-engine2021.appspot.com",
  messagingSenderId: "712071639712",
  appId: "1:712071639712:web:b304109521d18dd3e54fc5",
  measurementId: "G-10CQ22LSJ6"
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
