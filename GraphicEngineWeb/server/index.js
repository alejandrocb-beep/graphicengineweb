const express = require("express");
const cors = require("cors");
const bodyParser = require("body-parser");
const morgan = require("morgan");
const multer = require("multer");
const upload = multer({ dest: "uploads/" });

var fs = require('fs');


const app = express();

app.use(cors());
app.use(morgan("combined"));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.post("/upload", upload.single("photo"), (req, res) => {
  console.log(req.file);
  var at = req.file.originalname.indexOf("*");
  var nom = req.file.originalname.substring(at+1);
  req.file.originalname = req.file.originalname.slice(0,at);
  console.log(nom + " " + req.file.originalname);

  const dir = ".\\uploads\\"+nom;
  if (!fs.existsSync(dir)) {
    fs.mkdirSync(dir, {
      recursive: true
    });
  }

  fs.rename(".\\uploads\\"+req.file.filename, dir+"\\"+req.file.originalname, function(err) {
    if ( err ) console.log('ERROR: ' + err);
  });
});


//or you can also use get request
app.get('/download', function(req, res){
  var file = __dirname + '/uploads/alex.campanya.joliver@gmail.com/Ejemplo.ge';
  res.download(file);
});

const port = process.env.PORT || 3000;
app.listen(port, () => {
  console.log("Server running...");
});


var uri = "mongodb+srv://ies:ies@sandbox.nwcxt.mongodb.net/test";

const mongoose = require("mongoose");

const Schema = mongoose.Schema;

let usuarillicencia = new Schema(
  {
    correu: {
      type: String
    },
    llicencia: {
      type: Number
    },
    expira: {
      type: String
    }
  },
  { collection: "UsuariLlicencia" }
);

module.exports = mongoose.model("usuarillicencia", usuarillicencia);


mongoose.connect(uri, { useUnifiedTopology: true, useNewUrlParser: true });

const connection = mongoose.connection;

const router = express.Router();

app.use("/", router);
connection.once("open", function() {
  console.log("MongoDB database connection established successfully");
});

router.route("/insertdata").post(function(req, res) {});

var data = [
  {
    correu: "alex.campanya.joliver@gmail.com",
    llicencia: 21,
    expira: "11-11-11"
  },
  {
    correu: "nagacordeon@gmail.com",
    llicencia: 27,
    expira: "11-11-11"
  },
  {
    correu: "azul@es",
    llicencia: 23,
    expira: "11-11-11"
  }
];

usuarillicencia.insertMany(data, function(err, result) {
  if (err) {
    res.send(err);
  } else {
    res.send(result);
  }
});


router.route("/fetchdata").get(function(req, res) {
  usuarillicencia.find({}, function(err, result) {
    if (err) {
      res.send(err);
    } else {
      res.send(result);
    }
  });
});